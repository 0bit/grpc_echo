FROM python:3.7-alpine

ENV PYTHONUNBUFFERED 1

RUN apk update \
    # cryptography dependencies
    && apk add gcc g++ musl-dev python3-dev libffi-dev openssl-dev gettext-dev

RUN addgroup -g 1000 echo_user \
    && adduser -D -u 1000 -G echo_user echo_user

RUN pip3 install pipenv

RUN set -ex && mkdir /app
WORKDIR /app

COPY Pipfile Pipfile.lock /app/
RUN set -ex && pipenv install --deploy --system

COPY ./src /app
RUN chown -R echo_user:echo_user /app

USER echo_user

EXPOSE 50051

CMD ["python", "server.py"]
