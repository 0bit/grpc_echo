from generated import echo_pb2_grpc, echo_pb2


class EchoServicer(echo_pb2_grpc.EchoServicer):

    def UnaryEcho(self, request: echo_pb2.EchoRequest, context):
        message = request.message

        return echo_pb2.EchoResponse(message=message)
