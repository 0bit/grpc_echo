import time
from concurrent import futures

import grpc

from echo import EchoServicer
from generated import echo_pb2_grpc


def serve():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
    echo_pb2_grpc.add_EchoServicer_to_server(EchoServicer(), server)
    server.add_insecure_port('[::]:50051')
    server.start()
    while True:
        time.sleep(1000)


if __name__ == '__main__':
    serve()
